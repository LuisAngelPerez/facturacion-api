/*var admin = require("firebase-admin");
 
let id_facturas = ["-L7pSkHbwyBghjjQEHhe","-L7pUSM_jNuuTZN2W3Ar", "-L76eZ6RoZmI4XIwGrFS", "-L7TSJiI6Hpmmv2IvI6W", "-L7RgmzwguTOJbf4FS49"];
 
// Fetch the service account key JSON file contents
var serviceAccount = require("./config/service_accounts/admin-dev.json");
 
// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://reto3facturacion.firebaseio.com"
});
 
// As an admin, the app has access to read and write all data, regardless of Security Rules
var db = admin.database();*/


module.exports = function () {

    let msj_status = [];
    let db = require(global.rootPath('config/admin')).db
    let webhookWorker = require(global.rootPath('workers/webhooks'));
    let facturaJson = require(global.rootPath('serializers/factura-json'));


    function webhookRain(id_facturas) {
        let _ = require('underscore')

        if (!_.isArray(id_facturas)) {
            id_facturas = [id_facturas]
        }

        let promesaFacturas = id_facturas.map(function (factura) {
            return facturasArray(factura);
        })

        return Promise.all(promesaFacturas).then(() => {
            console.log(msj_status)
        }).catch((e) => {
            console.log("\nError------" + e)
        })
    }

    function facturasArray(factura) {

        var ref = db.ref(`facturas/${factura}`);

        return ref.once("value").then(function (snapshot) {
            var refe = db.ref(snapshot.val().path);
            return refe.once("value", function (snapshot) {

                msj_status.push(statusWebhooks(snapshot.val(), factura));

                return new facturaJson(snapshot).serialize().then(function (json) {
                    return webhookWorker.success(json.webhookUrl, json);
                });
            });
        }).catch((e) => console.log(e));

    }

    function statusWebhooks(snapFactura, factura) {
        var status = "", error = ""

        if (snapFactura.fechaTimbrado === undefined) {
            status = 3
            error = "No tiene existe campo fechaTimbrado"
        } else if (snapFactura.webhookUrl === undefined) {
            status = 3
            error = "No tiene existe campo Webhook"
        } else if (snapFactura.webhookUrl === "") {
            status = 3
            error = "No hay Url del Webhook";
        } else if (snapFactura.fechaTimbrado && snapFactura.prueba === false) {
            status = 2
        } else if (snapFactura.fechaTimbrado && snapFactura.prueba === true) {
            status = 3
            error = "Es de prueba";
        } else if (snapFactura.fechaTimbrado === "") {
            status = 3
            error = "No esta timbrada";
        }

        var obj;

        if (status !== 3) {
            obj = {
                id: factura,
                status: status
            }
        } else {
            obj = {
                id: factura,
                status: status,
                error: error
            }
        }

        return obj;
    }
    return {
        webhookRain: webhookRain
    }
}()