'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

module.exports = function() {
	// Constants
	let { url } = require('./constants')


	let path = require('path')
	let rootPath = path.resolve('')
	let fs = require('fs')

	let soap = require('soap')
	let request = require('request')

	let client = {}
	let clientOptions = {
		returnFault: true,
		disableCache: true,
		wsdl_options: {
			cert: fs.readFileSync(global.rootPath('config/zfactu/edicom.pem'))
		},
		request: request.defaults({
			agentOptions:{
				secureProtocol: 'TLSv1_method'
			}
		})
	}

	function createClient(){
		return soap.createClientAsync(url, clientOptions)
	}

	return {
		connect: createClient
	}
}()
